import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:sliver_wp_flutter/pages/HomePage.dart';

// APP启动->运行main->运行 runApp->运行MyApp 8.认识main.dart

void main() {
  //该方法执行一个runAPP方法，runApp方法需要传入一个 Widget 7.操刀main.dart
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //引入 ScreenUtilInit 教程 12.初始化依赖包 flutter_screenutil
    return ScreenUtilInit(
      minTextAdapt: true,//是否根据宽度/高度中的最小值适配文字
      splitScreenMode: true,
      designSize: const Size(360, 690), //设计稿中的屏幕尺
      builder: (BuildContext context, Widget? child) {
        return GetMaterialApp(
          //Get状态管理器 引入首页
          home: HomePage(),
        );
      },
    );
  }
}
