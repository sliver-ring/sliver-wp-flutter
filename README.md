# sliver-wp-flutter

#### 介绍
本仓库为WordPress+flutter开发APP教程flutter端代码存档仓库，教程地址以及使用详情请参看主页[WordPress+Flutter开发APP教程](https://wpcodeku.com/459.html)。

#### 软件架构
1. WordPress
2. flutter
3. sliver-wp-api(WordPress插件)
4. sliver-wp-app(WordPress插件))


#### 安装教程

1.  安装好WordPress网站
2.  在WordPress后台安装并启用sliver-wp-api、sliver-wp-app插件
3.  安装好flutter开发环境

#### 使用说明

参看主页[WordPress+Flutter开发APP教程](https://wpcodeku.com/459.html)。

#### 参与贡献

1.  在本仓库中提问
2.  在教程的每小节下提问